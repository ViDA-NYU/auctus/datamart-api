.. _query_result_schema:

Query Result Schema
*******************

.. literalinclude:: query_result_schema.json
   :language: json
   :linenos:

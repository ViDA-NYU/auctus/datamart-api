DataMart Python API: NYU
========================

The Python API provided by NYU is a wrapper around the `REST API <https://datadrivendiscovery.gitlab.io/datamart-api/rest_api.html>`__.

How to install?
---------------

::

    $ pip install datamart_nyu

How to use?
-----------

We provide an example using the `NY Taxi Demand data <https://gitlab.datadrivendiscovery.org/d3m/datasets/tree/master/seed_datasets_data_augmentation/DA_ny_taxi_demand/DA_ny_taxi_demand_dataset>`__. The example is available `here <examples/ny-taxi-demand.ipynb>`__.

DataMart API
============

This repository contains the specifications of the DataMart API, as well as a Python client.

The DataMart project is designed to be a web crawler and search engine for datasets, specifically meant for data augmentation tasks in machine learning. Its goal is to be able to find datasets in different repositories and index them for later retrieval.

See also our two implementations:

* [ISI's DataMart](https://github.com/usc-isi-i2/datamart)
* [NYU's DataMart](https://gitlab.com/ViDA-NYU/datamart/datamart)

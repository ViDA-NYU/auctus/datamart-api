.. _query:

Query Schema
************

The query API provides an interface for searching complementary datasets that can be used to augment the supplied dataset to improve prediction performance. It can be used either through the query endpoints of the :ref:`REST API <rest-api>`, or via the :ref:`Python API <python>`.

This API provides two ways to search for complementary datasets:

* Search by keywords
* Search by supplied data

In the former, the DataMart system will try to match datasets by keywords, named entities, temporal ranges, and geospatial ranges. In the latter, the DataMart system will try to find other datasets that can be used to augment the supplied, input dataset; query keywords can also be used for further filtering and ranking.

.. _query-input:

Input
=====

The API defines a query method that takes two inputs:

* `data`: input or supplied data
* `query`: a query JSON object

The JSON schema for `query` can be found :ref:`here <query_input_schema>`. Sample queries can be found :ref:`here <query_input_sample>`.

.. _query-output:

Output
======

The query returns a ranked list of dataset results in JSON. Each match contains a match score, and metadata of the matched dataset. The JSON schema for the query result can be found :ref:`here <query_result_schema>`. Sample results can be found :ref:`here <query_result_sample>`.
